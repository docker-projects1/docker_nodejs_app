FROM node:10-alpine
MAINTAINER Ginu Mathew1
RUN mkdir /home/node/nodejs
RUN chown -R node. /home/node
WORKDIR /home/node/nodejs
COPY package.json  /home/node/nodejs/
RUN npm install
COPY . .
EXPOSE 8080

CMD [ "node", "app.js" ]
